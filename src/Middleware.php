<?php


namespace Streetlamp;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use think\Debug;

abstract class Middleware
{
    final public function handle($name, &$args, \stdClass $context, \Closure $next)
    {
//        AnnotationRegistry::registerLoader('class_exists');
//        $reader          = new AnnotationReader();
//        $reflectionClass = new \ReflectionClass($context->method[0]);
//        $myAnnotation    = $reader->getClassAnnotations($reflectionClass);
//        $middlewares     = [];
//        foreach ($myAnnotation as $annotation) {
//            if ($annotation instanceof \app\library\annotation\Mapping\Middleware)
//                $middlewares = array_merge($middlewares, $annotation->getMiddleware());
//        }
//        $methodAnnotations = $reader->getMethodAnnotations($reflectionClass->getMethod($context->method[1]));
//        foreach ($methodAnnotations as $methodAnnotation) {
//            if ($methodAnnotation instanceof \app\library\annotation\Mapping\Middleware) {
//                $middlewares = array_merge($middlewares, $methodAnnotation->getMiddleware());
//            }
//        }
//        $middlewares = array_unique($middlewares);
//        if (!in_array(get_class($this), $middlewares))
//            return $next($name, $args, $context);
        $this->before($name, $args, $context);
        $result = $next($name, $args, $context);
        $this->after($name, $args, $context);
        return $result;
    }

    public function before($name, &$args, \stdClass $context)
    {

    }

    public function after($name, &$args, \stdClass $context)
    {

    }


}