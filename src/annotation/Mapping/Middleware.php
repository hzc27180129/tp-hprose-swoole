<?php


namespace Streetlamp\annotation\Mapping;

use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;

/**
 * Class Middleware
 * @package app\library\annotation\Mapping
 * @Annotation
 * @Attributes({
 *   @Attribute("middleware",type="array")
 * })
 * @Target({"CLASS","METHOD"})
 */
class Middleware
{
    /**
     * @var array
     */
    private $middleware;

    public function __construct(array $value)
    {
        if (isset($value['value']))
            $this->middleware = $value['value'];
        if (isset($value['middleware']))
            $this->middleware = $value['middleware'];
    }

    public function getMiddleware()
    {
        return $this->middleware;
    }
}