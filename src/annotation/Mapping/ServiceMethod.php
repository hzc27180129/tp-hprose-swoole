<?php


namespace Streetlamp\annotation\Mapping;


use Doctrine\Common\Annotations\Annotation\Attribute;

/**
 * Class ServiceMethod
 * @package library\annotation\Mapping
 * @Annotation
 * @Attributes  ({
 *     @Attribute("serviceName", type="string")
 * })
 * @Target ("METHOD")
 */
final class ServiceMethod
{
    /**
     * @Required ()
     */
    private $serviceName;

    public function __construct(array $value)
    {
        if (isset($value['value']))
            $this->serviceName = $value['value'];
        if (isset($value['serviceName']))
            $this->serviceName = $value['serviceName'];
    }

    public function getServiceName()
    {
        return $this->serviceName;
    }
}