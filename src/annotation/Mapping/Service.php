<?php

namespace Streetlamp\annotation\Mapping;


use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Service
 * @package app\library\annotation\Mapping
 * @Annotation
 * @Target("CLASS")
 */
final class Service
{

}