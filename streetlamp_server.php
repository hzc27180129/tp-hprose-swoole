<?php
return [
    'streetlamp' => [
        'module'      => 'script',//模块名。只有在模块的logic下的类才会被部署为微服务
        'host'        => '0.0.0.0',
        'port'        => 9001,
        'server_type' => 'swoole',//swoole socket两种
        'setting'     => [
            'worker_num'  => 1,
            'max_request' => 5000,
            'daemonize'   => false,
            'log_file'    => LOG_PATH . 'streetlamp/log.log'
        ],
        'middlewares' => [
            //            \app\script\middleware\TestaMiddleware::class,
            //            \app\script\middleware\TestMiddleware::class,
        ]
    ],

];